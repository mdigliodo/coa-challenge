import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private auth: AngularFireAuth) { }

  /*
  Registrar una nueva cuenta
  */
  async register(params: any) {
    try {
      return await this.auth.createUserWithEmailAndPassword(params.email, params.password);
    } catch (error) {
      return null;
    }
  }

  /*
    Loguear con email y pass
    */
  async loginIn(params: any) {
    try {
      return await this.auth.signInWithEmailAndPassword(params.email, params.password);
    } catch (error) {
      return null;
    }
  }

   /*
    Loguear con google
    */
  async loginInGoogle() {
    try {
      return await this.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
    } catch (error) {
      return null;
    }
  }

  // Obtenemos el usuario logueado
  getUserLog() {
    return this.auth.authState;
  }

  logout(): Promise<any> {
    return this.auth.signOut()
  }

}
