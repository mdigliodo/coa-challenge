import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Cart } from '../models/cart';
import { CartProduct } from '../models/cart_product';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ShopService {

  productsList: AngularFireList<Product>;
  cartList: AngularFireList<Cart>;
  cartProductList: AngularFireList<CartProduct>;

  constructor(private db: AngularFireDatabase) {
    this.productsList = this.db.list('products');
    this.cartList = this.db.list('cart');
    this.cartProductList = this.db.list('product_carts');
  }


  // Crud productos
  getProducts(): AngularFireList<Product> {
    return this.db.list('products');
  }

  getProductId(id: string){
    return this.db.list('products').query.endAt(id)
  }

  insertProduct(produt: Product) {
    this.productsList.push(produt)
  }

  updateProduct(id: string, p: Product) {
    this.productsList.update(id, p);
  }

  deleteProduct(id: string) {
    this.productsList.remove(id);
  }

  // Carro de compras
  getCarts(): AngularFireList<Cart> {
    return this.db.list('cart');
  }

  async insertCart(cart: Cart) {
    this.cartList.push(cart);
  }

  updateCart(id: string, c: Cart) {
    this.cartList.update(id, c);
  }

  deleteCart(id: string) {
    this.cartList.remove(id);
  }

  // Cart Product table
  getCartProduct(): AngularFireList<CartProduct> {
    return this.db.list('product_carts');
  }

  insertCartProduct(cartProduct: CartProduct) {
    this.cartProductList.push(cartProduct);
  }

  updateCartProduct(id: string, cp: CartProduct) {
    this.cartProductList.update(id, cp);
  }

  deleteCartProduct(id: string) {
    this.cartProductList.remove(id);
  }

}
