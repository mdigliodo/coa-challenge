import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable, Subscriber } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private auth: AuthService) {
    this.checkUserLogin();
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    return this.checkUserLogin();
  }


  async checkUserLogin(): Promise<boolean> {
    let result: boolean = false;
    await this.auth.getUserLog().subscribe(resp => {
      
      if(resp?.email){
        result = true;
      }else{
        result = false;
      }

    })

    return result;

  }

}
