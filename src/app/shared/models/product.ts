export class Product {
    id?: string | null;
    name?: string | null;
    price?: number | null;
    description?: string | null;

}