export class CartProduct {
    id?: string | null;
    product_id: string;
    cart_id: string;
    quantity: number;
    name?: string;

    constructor(cart_id: string, product_id: string, quantity: number){
        this.cart_id = cart_id;
        this.product_id= product_id;
        this.quantity = quantity;
    }
}