export class Cart {
    id?: string | null;
    status: 'pending' | 'complete';

    constructor(status: 'pending' | 'complete'){
        this.status = status;
    }

}