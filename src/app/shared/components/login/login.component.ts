import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formLogin: FormGroup = new FormGroup({});

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.createForm();
  }

  // Creamos el formulario de login
  createForm() {
    this.formLogin = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  /**
   * Login con email y password
   */
  loginIn() {
    this.authService.loginIn(this.formLogin.value).then(resp => {
      if (resp) {
        this.router.navigate(['/shop'])
      } else {
        Swal.fire({
          title: 'Email y/o contraseña ingresado es incorrecto!',
          timer: 2000,
          icon: 'error',
          showConfirmButton: false
        })
      }

    })
  }

  /**
   * Login con google
   */
  loginInGoogle() {
    this.authService.loginInGoogle().then(resp => {
      if (resp) {
        this.router.navigate(['/shop'])
      } else {
        Swal.fire({
          title: 'Error al iniciar sesión. Intente nuevamente más tarde!',
          timer: 2000,
          icon: 'error',
          showConfirmButton: false
        })
      }
    })
  }

}
