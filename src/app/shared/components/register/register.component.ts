import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  formRegistro: FormGroup = new FormGroup({});

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.createForm();
  }

  /**
    * crear formulario de registro
    */
  createForm() {
    this.formRegistro = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  /**
   * registrar nuevo usuario
   */
  register() {

    if (!this.formRegistro.valid) return;
    Swal.showLoading();

    this.authService.register(this.formRegistro.value).then((resp) => {
      Swal.fire({
        title: 'Registro exitoso!',
        timer: 2000,
        icon: 'success',
        showConfirmButton: false
      }).then(() => {
        this.router.navigate(['/login'])
      })

    })
  }

}
