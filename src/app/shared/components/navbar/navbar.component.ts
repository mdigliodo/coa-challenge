import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  usuarioLoggeado = this.auth.getUserLog();

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit(): void {

  }

  redirectTo(){
    this.usuarioLoggeado.subscribe(resp => {
      if(!resp) this.router.navigate(['/login']);
      this.router.navigate(['/shop'])
    })
  }

  logout() {
    this.auth.logout();
    this.router.navigate(['/login'])
  }

}
