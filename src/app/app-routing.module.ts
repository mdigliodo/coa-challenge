import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './shared/components/login/login.component';
import { RegisterComponent } from './shared/components/register/register.component';
import { ShopComponent } from './shop/components/shop/shop.component';
import { AngularFireAuthGuard } from '@angular/fire/auth-guard'
import { ProductListComponent } from './shop/components/product-list/product-list.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'shop', component: ShopComponent, canActivate: [AngularFireAuthGuard] },
  { path: 'products', component: ProductListComponent, canActivate: [AngularFireAuthGuard] },
  { path: '**', redirectTo: 'login' },
  { path: '', pathMatch: 'full', redirectTo: 'login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
