import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Cart } from 'src/app/shared/models/cart';
import { CartProduct } from 'src/app/shared/models/cart_product';
import { Product } from 'src/app/shared/models/product';
import { ShopService } from 'src/app/shared/services/shop.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {

  productList: any = [];
  productSelect: Product | undefined;
  cartList: Cart[] = [];
  cartSelected: Cart | undefined;
  cartProductList: CartProduct[] = [];
  listadoCartActual: any[] = [];

  constructor(private shopService: ShopService) { }

  ngOnInit(): void {
    /*
    Obtenemos el listado de carros de compras, productos y cart_products
    */
    this.getCart();
    this.getProducts();
    this.getProductCart();

  }


  // Añade un producto al carrito
  addProductCart(cart: Cart, p: Product) {
    let cartProductElement = this.cartProductList.find(element => element.cart_id == cart.id && element.product_id == p.id);
    if (cartProductElement) {
      //Si existe actualizo la cantidad de productos
      cartProductElement.quantity++;
      if (cartProductElement.id) this.shopService.updateCartProduct(cartProductElement.id, cartProductElement);
    } else {
      // Si no existe lo se agrega uno nuevo con cantidad 1
      if (!p.id) return;
      if (!cart.id) return;
      this.shopService.insertCartProduct({ cart_id: cart.id, product_id: p.id, quantity: 1 });
    }
  }

  /*
  Obtener listado de productos
  */
  getProducts() {
    this.shopService.getProducts().snapshotChanges().subscribe(items => {
      this.productList = [];
      items.forEach(element => {
        let product: any = element.payload.toJSON();
        product['id'] = element.key;
        this.productList.push(product as Product)
      })
    })
  }

  /*
  Obtener listado de productos
  */
  getProductCart() {
    this.shopService.getCartProduct().snapshotChanges().subscribe(items => {
      this.cartProductList = [];
      items.forEach(element => {
        let cartProduct: any = element.payload.toJSON();
        cartProduct['id'] = element.key;
        if (this.cartSelected && this.cartSelected.id == cartProduct.cart_id) {
          let nombreProduct = this.productList.find((e: any) => e.id == cartProduct.product_id);
          cartProduct['name'] = nombreProduct.name;
          this.cartProductList.push(cartProduct as CartProduct)
        }
      })
    })
  }

  /*
  Obtener listado de productos
  */
  getCart() {
    this.shopService.getCarts().snapshotChanges().subscribe(items => {
      this.cartList = [];
      items.forEach(element => {
        let cart: any = element.payload.toJSON();
        if (cart.status == 'pending') {
          cart['id'] = element.key;
          this.cartList.push(cart as Cart)
          this.cartSelected = cart;
        }
      })
    })
  }

  /*
    Agregamos un producto al carro de compras
  */
  newShop(p: Product) {
    if (!p.id) return;
    this.productSelect = p;
    // Tomamos el ultimo carro de compras pendiente
    let cart = this.cartList[this.cartList.length - 1]
    if (cart && cart.id && cart.status == 'pending') {
      this.addProductCart(cart, p);
    } else {
      this.shopService.insertCart({ status: 'pending' }).then(resp => {
        this.getCart();
        // Solucion provisoria
        // No llego a obtener el carro antes de agregar el nuevo product cart
        // Deberia utilizar asincronismo
        setTimeout(() => {
          if (this.cartSelected) {
            cart = this.cartSelected
            if (cart && cart.id && cart.status == 'pending') {
              this.addProductCart(cart, p);
            }
          }
        }, 500);
      })
    }
  }

 /*
    Completamos el carro de compras
  */
  shopNow() {
    if (this.cartSelected?.id) {
      this.cartSelected.status = 'complete';
      this.shopService.updateCart(this.cartSelected?.id, this.cartSelected)
      Swal.fire({
        title: 'Compra generada con exito!',
        timer: 2000,
        icon: 'success',
        showConfirmButton: false
      })
      this.cartProductList = [];
      this.shopService.insertCart({ status: 'pending' })
    }
  }

}
