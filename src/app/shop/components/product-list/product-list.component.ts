import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/shared/models/product';
import { ShopService } from 'src/app/shared/services/shop.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  productCart: any = [];
  productSelect: Product = new Product();

  constructor(private shopService: ShopService) { }

  ngOnInit(): void {
    // Obtenemos el listado de productos para editar/eliminar
    this.productCart = this.shopService.getProducts()
  }
  
  // Enviamos el producto seleccionado al formulario de edición
  editarProducto(p: any){
    if(!p) return;
    this.productSelect = p;
  }

}
