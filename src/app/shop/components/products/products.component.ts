import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Product } from 'src/app/shared/models/product';
import { ShopService } from 'src/app/shared/services/shop.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit, OnChanges {
  // Formulario reactivo de producto
  formProduct: FormGroup = new FormGroup({});
  // Nos determina si es para agregar/editar
  isEdit: boolean = false;

  // Recibimos el producto a editar
  @Input('editProduct') editProduct: Product | undefined;

  constructor(private formBuilder: FormBuilder, private shopService: ShopService) { }

  ngOnInit(): void {
    // Por defecto creamos el formulario de Agregar
    this.createForm();
  }

  ngOnChanges(changes: SimpleChanges): void {
    // Si recibimos un producto debemos crear un formulario de edicion
    if (changes.editProduct.currentValue.id) {
      this.createForm(changes.editProduct.currentValue);
    }

  }

  // Creamos el formulario editar/agregar
  createForm(product?: Product) {
    if (product?.id) {

      this.formProduct = this.formBuilder.group({
        id: [product.id],
        name: [product.name, Validators.required],
        price: [product.price, Validators.required],
        description: [product.description]
      })
      this.isEdit = true;
      return;
    }

    this.formProduct = this.formBuilder.group({
      id: [null],
      name: ['', Validators.required],
      price: ['', Validators.required],
      description: [null]
    })
    this.isEdit = false;

  }

  /*
    Add/edit un nuevo producto
  */
  addProduct() {

    // Obtenemos los valores del formulario producto
    let product: Product = this.formProduct.value;

    // Validamos que el formulario tenga los campos requeridos
    if (this.formProduct.invalid) {
      Swal.fire({
        title: `Error al ${product.id ? 'editar' : 'agregar'} productos!`,
        html: 'Faltan datos obligatorios',
        timer: 1500,
        icon: 'error',
        showConfirmButton: false
      })
      return;
    }

    // Editar/Agregar el producto
    if (product.id) {
      this.shopService.updateProduct(product.id, product)
    } else {
      this.shopService.insertProduct(product)
    }

    // Mensaje de exito
    Swal.fire({
      title: `Producto ${product.id ? 'editado' : 'agregado'} con éxito!`,
      timer: 2000,
      icon: 'success',
      showConfirmButton: false
    })
    // Reset form
    this.cleanForm();
  }

  // Reset form
  cleanForm() {
    this.formProduct.reset();
    this.isEdit = false;
    this.editProduct = undefined;
  }

}
