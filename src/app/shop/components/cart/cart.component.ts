import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Product } from 'src/app/shared/models/product';
import { ShopService } from 'src/app/shared/services/shop.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  @Output() editarProducto: EventEmitter<Product> = new EventEmitter<Product>()

  productList: Product[] = [];

  constructor(private shopService: ShopService) { }

  ngOnInit(): void {
    this.getProducts();
  }

  /*
    Obtenermos el listado de productos
  */
  getProducts() {
    this.shopService.getProducts().snapshotChanges().subscribe(items => {
      this.productList = [];
      items.forEach(element => {
        let product: any = element.payload.toJSON();
        product['id'] = element.key;
        this.productList.push(product as Product)
      })
    })
  }

  /*
    Editar producto del listado
  */
  editProduct(p: Product) {
    if (!p.id) return;
    this.editarProducto.emit(p);
  }

  /*
    Eliminar producto del listado
  */
  deleteProduct(p: Product) {
    if (!p.id) return;
    this.shopService.deleteProduct(p.id);
    Swal.fire({
      title: 'Producto eliminado con exito!',
      timer: 2000,
      icon: 'success',
      showConfirmButton: false
    })
  }

}
