// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCaCFb_bi3IKXPfMtTmzlyHfzbKUDiJNYE",
    authDomain: "coa-store.firebaseapp.com",
    projectId: "coa-store",
    storageBucket: "coa-store.appspot.com",
    messagingSenderId: "435580259203",
    appId: "1:435580259203:web:15ddab671cb025b0dac4b6"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
